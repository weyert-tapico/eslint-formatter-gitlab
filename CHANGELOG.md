# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project
adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [4.0.0] - 2022-11-08

### Changed

- Replace `js-yaml` with `yaml` for more correct YAML parsing.

### Removed

- Remove support for Node 12.

### Fixed

- Allow `!references` in `.gitlab-ci.yml`.

## [3.0.0] - 2021-10-17

### Added

- Support ESLint 8.

### Removed

- Remove support for Node 10.
- Remove config environment variable `ESLINT_FORMATTER`, always use builtin formatter instead.

## [2.2.0] - 2021-01-27

### Changed

- Updated `js-yaml`.

## [2.2.0] - 2020-12-12

### Added

- Added severity to output.

## [2.0.0] - 2020-04-14

### Changed

- Support ESLint 7.

### Fixed

- Fix infinite recursion when `ESLINT_CODE_QUALITY_REPORT` refers to `eslint-formatter-gitlab`
  itself.

## [1.1.0] - 2019-08-08

### Changed

- Support ESLint 6.

## [1.0.2] - 2018-12-13

### Fixes

- Fix automated release process.

## [1.0.1] - 2018-12-13

### Added

- Tests.
- Link to example merge request.

## [1.0.0] - 2018-11-29

### Added

- Initial release.
