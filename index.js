/**
 * @typedef {import('eslint').ESLint.LintResult} LintResult
 * @typedef {import('eslint').ESLint.LintResultData} LintResultData
 * @typedef {import('eslint').Linter.LintMessage} LintMessage
 */

/**
 * @typedef {object} GitLabReports
 * @property {string} [codequality]
 */

/**
 * @typedef {object} GitLabArtifacts
 * @property {GitLabReports} [reports]
 */

/**
 * @typedef {object} GitLabJob
 * @property {GitLabArtifacts} [artifacts]
 */

/**
 * @typedef {Record<string, GitLabJob>} GitLabCI
 */

/**
 * @typedef {object} CodeClimateLines
 * @property {number} begin
 * @property {number} end
 */

/**
 * @typedef {object} CodeClimateContents
 * @property {string} body
 */

/**
 * @typedef {object} CodeClimateLocation
 * https://github.com/codeclimate/platform/blob/master/spec/analyzers/SPEC.md#locations
 * @property {string} path
 * @property {CodeClimateLines} lines
 */

/**
 * @typedef {object} CodeClimateIssue
 * https://github.com/codeclimate/platform/blob/master/spec/analyzers/SPEC.md#issues
 * @property {'issue'} type
 * @property {string} check_name
 * @property {string} description
 * @property {CodeClimateContents} [contents]
 * @property {'info' | 'minor' | 'major' | 'critical' | 'blocker'} severity
 * @property {string} [fingerprint]
 * @property {CodeClimateLocation} location
 */

const { createHash } = require('node:crypto');
const { existsSync, lstatSync, mkdirSync, readFileSync, writeFileSync } = require('node:fs');
const { EOL } = require('node:os');
const { dirname, join, relative, resolve } = require('node:path');

const chalk = require('chalk');
const yaml = require('yaml');

const {
  CI_CONFIG_PATH = '.gitlab-ci.yml',
  CI_JOB_NAME,
  CI_PROJECT_DIR = process.cwd(),
  CI_PROJECT_URL,
  CI_COMMIT_SHORT_SHA,
  ESLINT_CODE_QUALITY_REPORT,
  GITLAB_CI,
  NODE_ENV,
} = process.env;

/**
 * @type {yaml.CollectionTag}
 */
const reference = {
  tag: '!reference',
  collection: 'seq',
  default: false,
  resolve() {
    // We only allow the syntax. We don’t actually resolve the reference.
  },
};

/**
 * @returns {string} The output path of the code quality artifact.
 */
function getOutputPath() {
  const configPath = join(CI_PROJECT_DIR, CI_CONFIG_PATH);
  // GitlabCI allows a custom configuration path which can be a URL or a path relative to another
  // project. In these cases CI_CONFIG_PATH is empty and we'll have to require the user provide
  // ESLINT_CODE_QUALITY_REPORT.
  if (!existsSync(configPath) || !lstatSync(configPath).isFile()) {
    throw new Error(
      'Could not resolve .gitlab-ci.yml to automatically detect report artifact path.' +
        ' Please manually provide a path via the ESLINT_CODE_QUALITY_REPORT variable.',
    );
  }
  const jobs = /** @type {GitLabCI} */ (
    yaml.parse(readFileSync(configPath, 'utf8'), { version: '1.1', customTags: [reference] })
  );
  const { artifacts } = jobs[/** @type {string} */ (CI_JOB_NAME)];
  const location = artifacts?.reports?.codequality;
  const msg = `Expected ${CI_JOB_NAME}.artifacts.reports.codequality to be one exact path`;
  if (!location) {
    throw new Error(`${msg}, but no value was found.`);
  }
  if (Array.isArray(location)) {
    throw new TypeError(`${msg}, but found an array instead.`);
  }
  return resolve(CI_PROJECT_DIR, location);
}

/**
 * @param {string} filePath The path to the linted file.
 * @param {LintMessage} message The ESLint report message.
 * @returns {string} The fingerprint for the ESLint report message.
 */
function createFingerprint(filePath, message) {
  const md5 = createHash('md5');
  md5.update(filePath);
  if (message.ruleId) {
    md5.update(message.ruleId);
  }
  md5.update(message.message);
  return md5.digest('hex');
}

/**
 * @param {LintResult[]} results The ESLint report results.
 * @param {LintResultData} data The ESLint report result data.
 * @returns {CodeClimateIssue[]} The ESLint messages in the form of a GitLab code quality report.
 */
function convert(results, data) {
  /** @type {CodeClimateIssue[]} */
  const messages = [];
  for (const result of results) {
    for (const message of result.messages) {
      const relativePath = relative(CI_PROJECT_DIR, result.filePath);

      /** @type {CodeClimateIssue} */
      const issue = {
        type: 'issue',
        check_name: message.ruleId ?? '',
        description: message.message,
        severity: message.severity === 2 ? 'major' : 'minor',
        fingerprint: createFingerprint(relativePath, message),
        location: {
          path: relativePath,
          lines: {
            begin: message.line,
            end: message.endLine ?? message.line,
          },
        },
      };
      const docs = message.ruleId ? data.rulesMeta[message.ruleId]?.docs : undefined;
      if (docs) {
        let body = docs.description || '';
        if (docs.url) {
          if (body) {
            body += '\n\n';
          }
          body += `[${message.ruleId}](${docs.url})`;
        }
        if (body) {
          issue.contents = { body };
        }
      }
      messages.push(issue);
    }
  }
  return messages;
}

/**
 * @param {LintMessage} message The ESLint report message.
 * @returns {boolean} `true` if the message is at error level, `false` if it represents a warning
 */
function messageIsLevelError(message) {
  return message.fatal || message.severity === 2;
}

/**
 * Make a text singular or plural based on the count.
 *
 * @param {number} count The count of the data.
 * @param {string} text The text to make singular or plural.
 * @returns {string} The formatted text.
 */
function plural(count, text) {
  return `${count} ${text}${count === 1 ? '' : 's'}`;
}

/**
 * @param {LintResult[]} results The ESLint report results.
 * @returns {string} The ESLint messages converted to a format
 * suitable as output in GitLab CI job logs.
 */
function gitlabConsoleFormatter(results) {
  // Severity labels manually padded to have equal lengths and end with spaces
  const labelError = `${chalk.red('error')}  `;
  const labelWarn = `${chalk.yellow('warn')}   `;

  const lines = [''];

  /** @type {string | undefined} */
  let gitLabBaseURL;
  if (CI_PROJECT_URL && CI_COMMIT_SHORT_SHA) {
    gitLabBaseURL = `${CI_PROJECT_URL}/-/blob/${CI_COMMIT_SHORT_SHA}/`;
  }

  let errors = 0;
  let warnings = 0;
  let maxRuleIdLength = 0;
  let maxMsgLength = 0;

  for (const result of results) {
    for (const message of result.messages) {
      const isError = messageIsLevelError(message);
      errors += isError ? 1 : 0;
      warnings += isError ? 0 : 1;
      maxRuleIdLength = message.ruleId
        ? Math.max(maxRuleIdLength, message.ruleId.length)
        : maxRuleIdLength;
      maxMsgLength = Math.max(maxMsgLength, message.message.length);
    }
  }

  for (const result of results) {
    const { filePath, messages } = result;
    const repoFilePath = relative(CI_PROJECT_DIR, filePath);

    for (const message of messages) {
      let line;
      line = messageIsLevelError(message) ? labelError : labelWarn;
      line += String(message.ruleId || '').padEnd(maxRuleIdLength + 2);
      line += message.message.padEnd(maxMsgLength + 2);

      if (gitLabBaseURL) {
        // Create link to referenced file in GitLab
        const anchor = message.line === undefined ? '' : `#L${message.line}`;
        line += chalk.blue(`${gitLabBaseURL}${repoFilePath}${anchor}`);
      } else {
        line += `${filePath}:${message.line || 0}:${message.column || 0}`;
      }

      lines.push(line);
    }
  }

  const total = warnings + errors;
  if (total > 0) {
    const details = `(${plural(errors, 'error')}, ${plural(warnings, 'warning')})`;
    lines.push('', `${chalk.red('✖')} ${plural(total, 'problem')} ${details}`);
  } else {
    lines.push(`${chalk.green('✔')} No problems found`);
  }

  lines.push('');
  return lines.join(EOL);
}

/**
 * @param {LintResult[]} results The ESLint report results.
 * @param {LintResultData} data The ESLint report result data.
 */
module.exports = (results, data) => {
  /* istanbul ignore next */
  if (GITLAB_CI === 'true' && NODE_ENV !== 'test') {
    chalk.level = 1;
  }
  if (CI_JOB_NAME || ESLINT_CODE_QUALITY_REPORT) {
    const issues = convert(results, data);
    const outputPath = ESLINT_CODE_QUALITY_REPORT || getOutputPath();
    const dir = dirname(outputPath);
    mkdirSync(dir, { recursive: true });
    writeFileSync(outputPath, JSON.stringify(issues, null, 2));
  }

  return gitlabConsoleFormatter(results);
};
