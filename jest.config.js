module.exports = {
  reporters: ['default', 'jest-junit'],
  coverageReporters: ['cobertura', 'json', 'lcov', 'text'],
};
