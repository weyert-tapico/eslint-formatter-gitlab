/**
 * @typedef {import('eslint').ESLint.LintResultData} LintResultData
 */

const { join } = require('node:path');

const { builtinRules } = require('eslint/use-at-your-own-risk');
const { fs, vol } = require('memfs');
const yaml = require('yaml');

/** @type {LintResultData} */
const lintResultData = {
  cwd: '/build',
  rulesMeta: {},
};
for (const [name, { meta }] of builtinRules.entries()) {
  if (meta) {
    lintResultData.rulesMeta[name] = meta;
  }
}

/**
 * @param {Record<string, string>} testEnv A mapping of environment variables to use in the test.
 * @param {object | string} gitlabCI An object which stubs the content of a `.gitlab-ci.yml` file.
 * @param {string} gitlabCIFile The location of the GitLab CI stub file.
 * @returns {Function} The patched module.
 */
function setup(testEnv, gitlabCI, gitlabCIFile = '/build/.gitlab-ci.yml') {
  process.env = testEnv;
  jest.restoreAllMocks();
  jest.resetModules();
  jest.spyOn(process, 'cwd').mockReturnValue('/build');
  jest.setMock('node:fs', fs);
  vol.reset();
  vol.fromJSON({
    [gitlabCIFile]: typeof gitlabCI === 'string' ? gitlabCI : yaml.stringify(gitlabCI),
  });
  return require('eslint-formatter-gitlab');
}

it('should write a code quality report', () => {
  const formatter = setup(
    {
      CI_JOB_NAME: 'test-lint-job',
    },
    {
      'test-lint-job': { artifacts: { reports: { codequality: 'output.json' } } },
    },
  );
  formatter(
    [
      {
        filePath: join(process.cwd(), 'filename.js'),
        messages: [
          { line: 42, message: 'Unexpected console statement', ruleId: 'no-console', severity: 2 },
          {
            line: 43,
            message: 'Unexpected debugger statement',
            ruleId: 'no-debugger',
            severity: 1,
          },
        ],
      },
    ],
    lintResultData,
  );
  expect(JSON.parse(/** @type {string} */ (vol.readFileSync('/build/output.json')))).toStrictEqual([
    {
      check_name: 'no-console',
      contents: {
        body: `Disallow the use of \`console\`

[no-console](https://eslint.org/docs/rules/no-console)`,
      },
      description: 'Unexpected console statement',
      fingerprint: '104f657895017428ff1fab2f7efd1c08',
      location: { lines: { begin: 42, end: 42 }, path: 'filename.js' },
      severity: 'major',
      type: 'issue',
    },
    {
      check_name: 'no-debugger',
      contents: {
        body: `Disallow the use of \`debugger\`

[no-debugger](https://eslint.org/docs/rules/no-debugger)`,
      },
      description: 'Unexpected debugger statement',
      fingerprint: 'e43578d636c9324207bb280b255ceffb',
      location: { lines: { begin: 43, end: 43 }, path: 'filename.js' },
      severity: 'minor',
      type: 'issue',
    },
  ]);
});

it('should throw if the output location is empty', () => {
  const formatter = setup(
    {
      CI_JOB_NAME: 'test-lint-job',
    },
    {
      'test-lint-job': { artifacts: { reports: { codequality: '' } } },
    },
  );
  expect(() => formatter([], lintResultData)).toThrow(
    new Error(
      'Expected test-lint-job.artifacts.reports.codequality to be one exact path, but no value was found.',
    ),
  );
});

it('should throw if the output location is an array', () => {
  const formatter = setup(
    {
      CI_JOB_NAME: 'test-lint-job',
    },
    {
      'test-lint-job': { artifacts: { reports: { codequality: [] } } },
    },
  );
  expect(() => formatter([], lintResultData)).toThrow(
    new Error(
      'Expected test-lint-job.artifacts.reports.codequality to be one exact path, but found an array instead.',
    ),
  );
});

it('should not fail if a rule id is null', () => {
  const formatter = setup(
    {
      CI_JOB_NAME: 'test-lint-job',
    },
    {
      'test-lint-job': { artifacts: { reports: { codequality: 'output.json' } } },
    },
  );
  formatter(
    [
      {
        filePath: join(process.cwd(), 'filename.js'),
        messages: [{ line: 42, message: 'This is a linting error', ruleId: null, severity: 2 }],
      },
    ],
    lintResultData,
  );
  expect(JSON.parse(/** @type {string} */ (vol.readFileSync('/build/output.json')))).toStrictEqual([
    {
      check_name: '',
      description: 'This is a linting error',
      fingerprint: '8e905c6c557d0066e5a01827b6216be6',
      location: { lines: { begin: 42, end: 42 }, path: 'filename.js' },
      severity: 'major',
      type: 'issue',
    },
  ]);
});

it('should not fail on partial rule metadata', () => {
  const formatter = setup(
    {
      CI_JOB_NAME: 'test-lint-job',
    },
    {
      'test-lint-job': { artifacts: { reports: { codequality: 'output.json' } } },
    },
  );
  formatter(
    [
      {
        filePath: join(process.cwd(), 'filename.js'),
        messages: [
          { line: 42, message: 'This is a linting error', ruleId: 'empty-docs', severity: 2 },
        ],
      },
      {
        filePath: join(process.cwd(), 'filename.js'),
        messages: [
          { line: 42, message: 'This is a linting error', ruleId: 'no-description', severity: 2 },
        ],
      },
      {
        filePath: join(process.cwd(), 'filename.js'),
        messages: [{ line: 42, message: 'This is a linting error', ruleId: 'no-url', severity: 2 }],
      },
    ],
    {
      cwd: '/build',
      rulesMeta: {
        'empty-docs': {
          docs: {},
        },
        'no-description': {
          docs: { url: 'https://example.com' },
        },
        'no-url': {
          docs: { description: 'Description only' },
        },
      },
    },
  );
  expect(JSON.parse(/** @type {string} */ (vol.readFileSync('/build/output.json')))).toStrictEqual([
    {
      check_name: 'empty-docs',
      description: 'This is a linting error',
      fingerprint: '892f75d009ce0ded668e1b6bb99c9f55',
      location: { lines: { begin: 42, end: 42 }, path: 'filename.js' },
      severity: 'major',
      type: 'issue',
    },
    {
      check_name: 'no-description',
      contents: { body: '[no-description](https://example.com)' },
      description: 'This is a linting error',
      fingerprint: 'b230d69d1534c076b20af576036459b5',
      location: { lines: { begin: 42, end: 42 }, path: 'filename.js' },
      severity: 'major',
      type: 'issue',
    },
    {
      check_name: 'no-url',
      contents: { body: 'Description only' },
      description: 'This is a linting error',
      fingerprint: '1dd6e71cf9acd1aef18bf8374e752ab8',
      location: { lines: { begin: 42, end: 42 }, path: 'filename.js' },
      severity: 'major',
      type: 'issue',
    },
  ]);
});

it('should skip the output if CI_JOB_NAME is not defined', () => {
  const formatter = setup(
    {},
    {
      'test-lint-job': { artifacts: { reports: { codequality: 'output.json' } } },
    },
  );
  formatter([], lintResultData);
  expect(vol.existsSync('/build/output.json')).toBe(false);
});

it('should respect the ESLINT_CODE_QUALITY_REPORT environment variable', () => {
  const formatter = setup(
    {
      ESLINT_CODE_QUALITY_REPORT: 'elsewhere.json',
    },
    {
      'test-lint-job': { artifacts: { reports: { codequality: 'output.json' } } },
    },
  );
  formatter([], lintResultData);
  expect(
    JSON.parse(/** @type {string} */ (vol.readFileSync('/build/elsewhere.json'))),
  ).toStrictEqual([]);
});

it('should provide a report with URLs', () => {
  const formatter = setup(
    {
      CI_JOB_NAME: 'test-lint-job',
      CI_PROJECT_URL: 'https://gitlab.example.com/test/project',
      CI_COMMIT_SHORT_SHA: 'abc12345ef',
    },
    {
      'test-lint-job': { artifacts: { reports: { codequality: 'output.json' } } },
    },
  );
  const result = formatter(
    [
      {
        filePath: join(process.cwd(), 'filename.js'),
        errorCount: 1,
        warningCount: 0,
        messages: [
          {
            line: 42,
            column: 3,
            message: 'This is a linting error',
            ruleId: 'linting-error',
            severity: 2,
          },
          { message: 'This is a linting warning', ruleId: 'linting-warning', severity: 1 },
        ],
      },
    ],
    lintResultData,
  );

  expect(result).toBe(`
error  linting-error    This is a linting error    https://gitlab.example.com/test/project/-/blob/abc12345ef/filename.js#L42
warn   linting-warning  This is a linting warning  https://gitlab.example.com/test/project/-/blob/abc12345ef/filename.js

✖ 2 problems (1 error, 1 warning)
`);
});

it('should not try to create URLs when CI environment vars are not set', () => {
  const formatter = setup(
    {
      CI_JOB_NAME: 'test-lint-job',
    },
    {
      'test-lint-job': { artifacts: { reports: { codequality: 'output.json' } } },
    },
  );
  const result = formatter(
    [
      {
        filePath: join(process.cwd(), 'filename.js'),
        errorCount: 1,
        warningCount: 0,
        messages: [
          { line: 42, message: 'This is a linting error', ruleId: 'linting-error', severity: 2 },
        ],
      },
      {
        filePath: join(process.cwd(), 'filename.js'),
        errorCount: 1,
        warningCount: 0,
        messages: [
          { column: 42, message: 'This is a linting error', ruleId: 'linting-error', severity: 2 },
        ],
      },
    ],
    lintResultData,
  );

  expect(result).toBe(`
error  linting-error  This is a linting error  /build/filename.js:42:0
error  linting-error  This is a linting error  /build/filename.js:0:42

✖ 2 problems (2 errors, 0 warnings)
`);
});

it('should print a message when results are empty', () => {
  const formatter = setup(
    {
      CI_JOB_NAME: 'test-lint-job',
    },
    {
      'test-lint-job': { artifacts: { reports: { codequality: 'output.json' } } },
    },
  );
  const result = formatter([], lintResultData);

  expect(result).toBe(`
✔ No problems found
`);
});

it('should throw if CI_CONFIG_PATH is empty', () => {
  const formatter = setup(
    {
      CI_CONFIG_PATH: '',
      CI_JOB_NAME: 'test-lint-job',
    },
    {
      'test-lint-job': { artifacts: { reports: { codequality: 'output.json' } } },
    },
  );
  expect(() => formatter([], lintResultData)).toThrow(
    new Error(
      'Could not resolve .gitlab-ci.yml to automatically detect report artifact path.' +
        ' Please manually provide a path via the ESLINT_CODE_QUALITY_REPORT variable.',
    ),
  );
});

it('should not fail on !reference', () => {
  const formatter = setup(
    {
      CI_JOB_NAME: 'test-lint-job',
    },
    `
test-lint-job:
  artifacts:
    reports:
      codequality: output.json

misc: !reference [test-lint-job]
`,
  );
  const result = formatter([], lintResultData);
  expect(result).toBe('\n✔ No problems found\n');
});
